## justCTF 2020

* **CTF Time page:** https://ctftime.org/event/1050
* **Category:** Jeopardy
* **Date:** Sat, 30 Jan. 2021, 06:00 UTC — Sun, 31 Jan. 2021, 19:00 UTC

Note: This CTF was originally planned for 2020 but was postponed and played in 2021

---

### Solved Challenges
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|that's not crypto|rev|50|python, rev, bruteforce|
