## that's not crypto

* **CTF:** justCTF 2020
* **Category:** jeopardy
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 31/01/21

---

### Challenge
```
That's not crypto

This is very simple RE task, but you may need some other skills as well. :)
```

### Downloads
* [checker.pyc](checker.pyc)

---

### Solution
The provided program prompts the user the enter the flag, checks your input and tells you whether you are correct. Our job is to reverse the binary to see if we can figure out the flag from the program itself.

I decompiled the python bytecode: `uncompyle6 checker.pyc > checker.py`.

The source code is long, predominantly filled with a large array of long numbers that must contain our encrypted flag. There are 2 main driver functions.

The `main` function:

```python
flag_str = input('flag: ').strip()
flag = make_correct_array(flag_str)
if validate(a, flag):
    print('Yes, this is the flag!')
    print(flag_str)
else:
    print('Incorrect, sorry. :(')
```

And the `validate` function.

```python
def validate(a, xs):

    def poly(a, x):
        value = 0
        for ai in a:
            value *= x
            value += ai

        return value

    if len(a) != len(xs) + 1:
        return False
    else:
        for x in xs:
            value = poly(a, x)
            if value != 24196561:
                return False

        return True
```

* The `main` function just prompts for input, passes it to `validate` and then prints the result.
* The `validate` function:
	* first checks that the length of our flag guess (`xs`) matches the length of the actual flag (`a`)
	* then compares each letter sequentially and individually to see if they match. If any comparison fails, it returns immediately.

Some things we know about the flag already:
* We know the flag will start with the 8 characters `justCTF{`
* By printing out the length of `a` in the `validate` function (`print(len(a))`) we know that the flag is 57 characters long

My solution was to:
* Start with a known flag string of `justCTF{`
* Go through every printable character and append it to the end of our known string, filling the rest of the string with junk bits to make up our required length of 57 characters in the guess attempt. `justCTF{aaaaaaa...}`, `justCTF{baaaaaa...}`, `justCTF{caaaaaa...}`, `justCTF{daaaaaa...}`, etc.
* Inside our validate function, count the number of correct characters that we get right before the comparison fails and return that number.
* If that return number is greater than the length of our current "known flag string", then the last character that we appended must be correct.
* Append that character to our known flag string and start the process again, stopping only when we reach a length 57 characters.
* Print the flag.

Here are the modifications I made to the `main` and `validate` functions:

Main:

```python
flag = "justCTF{"
known = 8

while(known <= 56):
    fill = 57 - known
    for c in string.printable:
        flag_str = flag + c
        flag_str = flag_str + ('a' * (fill - 1))
        flag_test = make_correct_array(flag_str)
        if(validate(a, flag_test) > known):
            flag += c
            known += 1
print(flag)
```

Validate:

```python
def validate(a, xs):

    def poly(a, x):
        value = 0
        for ai in a:
            value *= x
            value += ai

        return value

    correct = 0

    if len(a) != len(xs) + 1:
        return 1
    else:
        for x in xs:
            value = poly(a, x)
            if value != 24196561:
                return correct
            else:
                correct += 1

        return correct
```
Running the program prints the flag in about 1 second.

---

### Flag 
```
justCTF{this_is_very_simple_flag_afer_so_big_polynomails}
```
